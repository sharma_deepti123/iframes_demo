if(typeof(jQuery) == 'undefined'){ 
	document.write("<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>"); 
}
var iframe_obj = (function() {
	
	function add_product(product){
		$.ajax({
			url: 'http://localhost:3000/save_product',
			type: 'POST',
			data: {product: product},
			success: function(){
				location.reload();
			}
		})
	}

	function initiate_frame(){
		$.ajax({
			url: 'http://localhost:3000/widget.js',
			type: 'GET',
			data: {}
		})
	}

	function triggerDeleteEvent() {
		$.event.trigger({
			type: "product_deleted",
			message: "event for deleted product",
			time: new Date()
		})
	}
       

    return {
        add_product: add_product,
       	initiate_frame: initiate_frame
    };
})();


